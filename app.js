const express = require('express')
const mongoose = require('mongoose')
var cors = require('cors')
var bodyParser = require('body-parser')
const UserController = require('./controllers/UserController')
const VisitController = require('./controllers/VisitController')
// DB
mongoose.set('useFindAndModify', false)
mongoose
  .connect(
    'mongodb+srv://admin:Y1A8EjHKndHmU9ff@cluster0.imxe9.mongodb.net/rirs?retryWrites=true&w=majority',
    {
      useNewUrlParser: true
    }
  )
  .then(() => console.log('Mongo DB | Connected'))
  .catch((error) => console.log(error))

const app = express()
app.use(cors())
app.use(bodyParser.json())
const port = process.env.PORT || 3000

//Routes
app.get('/user/:id', UserController.get)
app.post('/user/create', UserController.create)
app.put('/user/:id', UserController.update)
app.delete('/user', UserController.delete)

app.get('/visits', VisitController.get)
app.get('/visits/:id', VisitController.getOne)
app.get('/visits/user/:id', VisitController.getUser)
app.post('/visit/create/:id', VisitController.create)
app.listen(port, () => {
  console.log(`App listening on http://localhost:${port}`)
})

// USER (Get (Clients only),Create, Delete, Update)
// - name: String
// - surname: String
// - type: Client/Personal
// - visits: Array (Client only)

// VISIT (Create, Delete, Update)
// - USER: Ref
// - description: String
// - date: DateTime
// - carType: String
