const User = require('../models/User')
const Visit = require('../models/Visit')

const UserController = {
  get: async (req, res) => {
    const result = await User.findById(req.params.id)
      .populate('visits')
      .catch(() => {
        return res.status(404).json({ message: 'User not found' })
      })
    res.json(result)
  },
  create: async (req, res) => {
    const user = await User.create(req.body).catch(() => {
      return res.status(404).json({ message: 'User could not be created' })
    })
    res.json(user)
  },
  update: async (req, res) => {
    const user = await User.findOneAndUpdate({ _id: req.params.id }, req.body, {
      new: true
    }).catch(() => {
      return res.status(404).json({ message: 'User could not be updated' })
    })
    res.json(user)
  },
  delete: async (req, res) => {
    if (!req.body.id) return res.status(400).json({ message: 'Missing ID' })
    await Visit.find({ client: req.body.id })
      .remove()
      .catch(() => {
        return res.status(404).json({ message: 'Visits could not be deleted' })
      })
    await User.findOneAndDelete(req.body.id).then(() =>
      res.status(200).json({ message: 'User deleted' })
    )
  }
}

module.exports = UserController
