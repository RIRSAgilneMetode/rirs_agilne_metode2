const User = require('../models/User')
const Visit = require('../models/Visit')

const VisitController = {
  get: async (req, res) => {
    const result = await Visit.find().catch(() => {
      return res.status(404).json({ message: 'Visits not found' })
    })
    res.json(result)
  },
  getOne: async (req, res) => {
    const result = await Visit.findById(req.params.id).catch(() => {
      return res.status(404).json({ message: 'Visit not found' })
    })
    res.json(result)
  },
  getUser: async (req, res) => {
    const user = await User.findById(req.params.id)
      .populate('visits')
      .catch(() => {
        return res.status(404).json({ message: 'Visit not found' })
      })

    res.json(user)
  },
  create: async (req, res) => {
    let newVisit = new Visit(req.body)
    const user = await User.findById(req.params.id).catch((error) => {
      return res.status(404).json({ message: 'User not found' })
    })
    newVisit.client = user
    await newVisit.save().catch((error) => console.log(error))
    user.visits.push(newVisit)
    await user.save().catch((error) => console.log(error))
    res.status(200).json(newVisit)
  }
}

module.exports = VisitController
