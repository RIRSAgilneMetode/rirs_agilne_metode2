const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  name: String,
  surname: String,
  isServicer: Boolean,
  visits: [
    {
      type: Schema.Types.ObjectId,
      ref: 'visit'
    }
  ]
})

module.exports = User = mongoose.model('user', UserSchema)
