const mongoose = require('mongoose')
const Schema = mongoose.Schema

const VisitSchema = new Schema({
  date: String,
  description: String,
  carType: String,
  client: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  }
})

module.exports = Transaction = mongoose.model('visit', VisitSchema)
